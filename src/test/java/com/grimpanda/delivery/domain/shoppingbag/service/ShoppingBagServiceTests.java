package com.grimpanda.delivery.domain.shoppingbag.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.grimpanda.delivery.DeliveryApplication;
import com.grimpanda.delivery.domain.shoppingbag.model.Artwork;
import com.grimpanda.delivery.domain.shoppingbag.model.Buyer;
import com.grimpanda.delivery.domain.shoppingbag.model.Item;
import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;

@SpringBootTest
@ContextConfiguration(classes = DeliveryApplication.class)
@Transactional
public class ShoppingBagServiceTests {
    @Autowired
    private ShoppingBagService shoppingBagService;

    Artwork[] artworks = {
        new Artwork(null, "Sample Artwork1",  "Author"), 
        new Artwork(null, "Sample Artwork2",  "Author")
    };
    Item[] items = {
        new Item(null, "Sample Item1",  "Supplier"),
        new Item(null, "Sample Item1",  "Supplier")
    };
    Buyer buyer = new Buyer("Buyer", "01012345678", "test@test.com");

    @Test
    public void 장바구니_담김() {
        //given
        ShoppingBagProduct product = new ShoppingBagProduct(null, artworks[0], items[0], Long.valueOf(500), 2, buyer);

        //when
        shoppingBagService.addProductToShoppingBag(product);
        List<ShoppingBagProduct> shoppingBag = shoppingBagService.getShoppingBagProductsByName(buyer.getName());

        //then
        assertEquals(1, shoppingBag.size());
    }

    @Test
    public void 장바구니_1개_삭제() {
        //given
        ShoppingBagProduct product = new ShoppingBagProduct(null, artworks[0], items[0], Long.valueOf(500), 2, buyer);
        shoppingBagService.addProductToShoppingBag(product);
        List<ShoppingBagProduct> shoppingBagBeforeRemove = shoppingBagService.getShoppingBagProductsByName(buyer.getName());
        ShoppingBagProduct productToRemove = shoppingBagBeforeRemove.get(0);

        //when
        shoppingBagService.removeProductFromShoppingBag(productToRemove);
        List<ShoppingBagProduct> shoppingBagAfterRemove = shoppingBagService.getShoppingBagProductsByName(buyer.getName());

        //then
        assertEquals(shoppingBagBeforeRemove.size(), 1);
        assertEquals(shoppingBagAfterRemove.size(), 0);
    }
    
    
    @Test
    public void 장바구니_n개_삭제() {
        //given
        for (Artwork artwork : artworks) {
            for (Item item : items) {
                shoppingBagService.addProductToShoppingBag(new ShoppingBagProduct(null, artwork, item, Long.valueOf(500), 2, buyer));
            }
        }
        List<ShoppingBagProduct> shoppingBagBeforeRemove = shoppingBagService.getShoppingBagProductsByName(buyer.getName());
        List<ShoppingBagProduct> productsToRemove = new ArrayList<>(shoppingBagBeforeRemove.subList(0, 2));

        //when
        shoppingBagService.removeProductsFromShoppingBag(productsToRemove);
        List<ShoppingBagProduct> shoppingBagAfterRemove = shoppingBagService.getShoppingBagProductsByName(buyer.getName());

        //then
        assertEquals(shoppingBagBeforeRemove.size(), artworks.length * items.length);
        assertEquals(shoppingBagAfterRemove.size(), shoppingBagBeforeRemove.size() - productsToRemove.size());
    }
}
