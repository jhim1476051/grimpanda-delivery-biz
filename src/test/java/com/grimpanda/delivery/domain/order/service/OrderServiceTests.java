package com.grimpanda.delivery.domain.order.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.grimpanda.delivery.DeliveryApplication;
import com.grimpanda.delivery.domain.order.model.Address;
import com.grimpanda.delivery.domain.order.model.Buyer;
import com.grimpanda.delivery.domain.order.model.Order;
import com.grimpanda.delivery.domain.order.model.ShippingInfo;
import com.grimpanda.delivery.domain.order.model.Status;

@SpringBootTest
@ContextConfiguration(classes = DeliveryApplication.class)
@Transactional
public class OrderServiceTests {
    @Autowired
    private OrderService orderService;

    Buyer buyer = new Buyer("Buyer", "01012345678", "test@test.com");

    @Test
    public void 주문정보_저장_후_조회() {
        //given
        Order order = new Order(null, Status.결제_전, null, new ShippingInfo(new Address("00000", "서울특별시 중구 을지로10길 9", "1003호"), ""), buyer, null);

        //when
        orderService.saveOrder(order);
        Order result = orderService.findOrderBeforePayment(buyer.getName()).orElse(null);

        //then
        assertNotNull(result);
    }
}
