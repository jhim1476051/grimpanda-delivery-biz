package com.grimpanda.delivery.interfaces.controller;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class OrderBeforePaymentControllerTests {
    
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetOrderBeforePayment() throws Exception {
        mockMvc
        .perform(MockMvcRequestBuilders.get("/orders/before-payment"))
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        ;
    }

    @Test
    void testSaveOrderBeforePayment() throws Exception {
        Map<String, String> dto = new HashMap<>();
        dto.put("zipCode", "00000");
        dto.put("roadName", "을지로10길 9");
        dto.put("addressDetail", "1003호");
        dto.put("remark", "없음");
        
        String content = objectMapper.writeValueAsString(dto);
        
        mockMvc
        .perform(MockMvcRequestBuilders
            .post("/orders/before-payment")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(MockMvcResultMatchers.status().isOk())
        ;
    }
    
    @Test
    void testApproveOrder() throws Exception {
        mockMvc
        .perform(MockMvcRequestBuilders.post("/orders/before-payment/approve"))
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        ;
    }
}
