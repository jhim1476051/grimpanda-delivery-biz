package com.grimpanda.delivery.interfaces.dto;

import com.grimpanda.delivery.domain.delivery.model.Buyer;
import com.grimpanda.delivery.domain.delivery.model.Product;
import com.grimpanda.delivery.domain.delivery.model.ShippingInfo;
import com.grimpanda.delivery.domain.delivery.model.DeliveryStatus;

import lombok.Data;

@Data
public class DeliveryDto {
    private Long invoiceNum;
    private Product product;
    private DeliveryStatus status;
    private ShippingInfo shippingInfo;
    private Buyer buyer; 
}
