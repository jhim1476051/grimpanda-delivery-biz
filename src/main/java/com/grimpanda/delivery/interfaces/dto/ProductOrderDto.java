package com.grimpanda.delivery.interfaces.dto;

import com.grimpanda.delivery.domain.delivery.model.Buyer;
import com.grimpanda.delivery.domain.delivery.model.Product;
import com.grimpanda.delivery.domain.delivery.model.ShippingInfo;
import com.grimpanda.delivery.domain.order.model.Artwork;
import com.grimpanda.delivery.domain.order.model.Item;
import com.grimpanda.delivery.domain.order.model.ProductOrderStatus;
import com.grimpanda.delivery.domain.delivery.model.DeliveryStatus;

import lombok.Data;
import lombok.Setter;

@Data
public class ProductOrderDto {
    private Long poId;
    private Artwork artwork; 
    private Item item;
    private Long unitPrice;
    private Integer amount;
    private ProductOrderStatus status;
}
