package com.grimpanda.delivery.interfaces.dto;

import lombok.Getter;

@Getter
public class OrderBeforePaymentDto {
    private String zipCode;
    private String roadName;
    private String addressDetail;
    private String remark;

    @Override
    public String toString() {
        return zipCode + roadName + addressDetail + remark;
    }
}
