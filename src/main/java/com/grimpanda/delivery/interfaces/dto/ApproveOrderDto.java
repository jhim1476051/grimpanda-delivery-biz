package com.grimpanda.delivery.interfaces.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ApproveOrderDto {
    private Long orderId;
    private String paymentKey;
    private String tossPaymentOrderId;
    private Integer amount;
}
