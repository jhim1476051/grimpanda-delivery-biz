package com.grimpanda.delivery.interfaces.dto;

import com.grimpanda.delivery.domain.shoppingbag.model.ArtworkInfo;
import com.grimpanda.delivery.domain.shoppingbag.model.ProductInfo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShoppingbagInfoDto {

    private ShoppingbagInfoDto() {
        throw new IllegalStateException("Parent DTO class");
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Request for sending to Shopping bag")
    public static class RegisterRequest {
        @Schema(description = "artworkId", example = "1")
        private Integer artworkId;

        @Schema(description = "goodsId", example = "1")
        private Integer goodsId;

        @Schema(description = "amount", example = "3")
        private Integer amount;
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Builder
    @AllArgsConstructor
    @Schema(title = "Sending to Shopping bag Detail Info")
    public static class RequestDetail {
        @Schema(description = "artwork")
        private ArtworkInfo artwork;

        @Schema(description = "product")
        private ProductInfo product;

        @Schema(description = "amount")
        private Integer amount;
    }
}
