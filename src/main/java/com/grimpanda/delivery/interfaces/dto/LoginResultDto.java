package com.grimpanda.delivery.interfaces.dto;


import com.grimpanda.delivery.domain.member.model.MemberType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginResultDto{

    private Boolean login;
    private String email;
    private String hpNo;
    private String name;
    private String nickname;
    private MemberType memberType;
    private Boolean status;
}
