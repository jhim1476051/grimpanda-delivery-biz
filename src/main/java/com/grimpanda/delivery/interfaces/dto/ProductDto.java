package com.grimpanda.delivery.interfaces.dto;

import com.grimpanda.delivery.domain.shoppingbag.model.Artwork;
import com.grimpanda.delivery.domain.shoppingbag.model.Item;

import lombok.Getter;

@Getter
public class ProductDto {
    private Long id;
    private Artwork artwork;
    private Item item;
    private Long unitPrice;
    private Integer amount;
}
