package com.grimpanda.delivery.interfaces.controller;

import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ORDERBEFOREPAYMENT;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ORDERDELIVERY;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.USER;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grimpanda.delivery.common.feign.member.MemberFeignClient;
import com.grimpanda.delivery.domain.order.model.Address;
import com.grimpanda.delivery.domain.order.model.Buyer;
import com.grimpanda.delivery.domain.order.model.Order;
import com.grimpanda.delivery.domain.order.model.ShippingInfo;
import com.grimpanda.delivery.domain.order.model.Status;
import com.grimpanda.delivery.domain.order.service.OrderService;
import com.grimpanda.delivery.interfaces.dto.ApproveOrderDto;
import com.grimpanda.delivery.interfaces.dto.LoginResultDto;
import com.grimpanda.delivery.interfaces.dto.OrderBeforePaymentDto;

import io.swagger.v3.oas.annotations.Operation;

@RestController
//@RequestMapping("/orders/before-payment")
@RequestMapping(ORDERDELIVERY)
public class OrderBeforePaymentController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private MemberFeignClient memberFeignClient;
    
    @Operation(summary = "Get Orders BeforePayment Data",
            tags = {"OrderBeforePayment API"},
            description = "Get Orders BeforePayment Data")
    @GetMapping(
    		value = USER + ORDERBEFOREPAYMENT
    		)
    public ResponseEntity<?> getOrderBeforePayment(@PathVariable(value = "tenantId") String userId) {
        Optional<Order> order = orderService.findOrderBeforePayment(userId);

        return ResponseEntity.of(order);
    }

    @Operation(summary = "Add Orders BeforePayment Data",
            tags = {"OrderBeforePayment API"},
            description = "Add Orders BeforePayment Data")
    @PostMapping(value = USER + ORDERBEFOREPAYMENT)
    public ResponseEntity<?> saveOrderBeforePayment(@PathVariable(value = "tenantId") String userId, @RequestBody OrderBeforePaymentDto orderBeforePayment) {
        Optional<Order> order = orderService.findOrderBeforePayment(userId);
        
        LoginResultDto buyerInfo = memberFeignClient.getMember(userId); 
        Buyer buyer = new Buyer(buyerInfo.getNickname(), buyerInfo.getHpNo(), buyerInfo.getEmail());

        ShippingInfo newShippingInfo = new ShippingInfo(
                new Address(orderBeforePayment.getZipCode(), orderBeforePayment.getRoadName(),
                        orderBeforePayment.getAddressDetail()),
                orderBeforePayment.getRemark());

        Order result;
        
        try {
            Order newOrder = order.get();
            newOrder.setShippingInfo(newShippingInfo);

            result = orderService.saveOrder(newOrder);
        } catch (NoSuchElementException e) {
            Order newOrder = Order.builder().shippingInfo(newShippingInfo).buyer(buyer).status(Status.결제_전).build();

            result = orderService.saveOrder(newOrder);
        }

        return ResponseEntity.ok(result);
    }

    @Operation(summary = "Approve Orders",
            tags = {"OrderBeforePayment API"},
            description = "Approve Orders")
    @PostMapping(
    		value = USER + ORDERBEFOREPAYMENT	+ "/approve"
    		)
    public ResponseEntity<?> approveOrderAfterPayment(@PathVariable(value = "tenantId") String userId, @RequestBody ApproveOrderDto approveOrderDto) {
    	LoginResultDto buyerInfo = memberFeignClient.getMember(userId);       
    	Buyer buyer = new Buyer(buyerInfo.getNickname(), buyerInfo.getHpNo(), buyerInfo.getEmail());
        orderService.approveOrder(buyer, approveOrderDto);

        return ResponseEntity.ok(null);
    }
}
