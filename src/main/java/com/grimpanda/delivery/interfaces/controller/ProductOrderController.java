package com.grimpanda.delivery.interfaces.controller;

import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ALL;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ORDERDELIVERY;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.PRODUCTORDER;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.SAVE;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.USER;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.grimpanda.delivery.domain.order.model.ProductOrder;
import com.grimpanda.delivery.domain.order.model.ProductOrderStatus;
import com.grimpanda.delivery.domain.order.service.ProductOrderService;
import com.grimpanda.delivery.interfaces.dto.ProductOrderDto;

import io.swagger.v3.oas.annotations.Operation;


@RestController
@RequestMapping(ORDERDELIVERY)
public class ProductOrderController {
    
	
    @Autowired
    private ProductOrderService productOrderService;

    //발주정보 조회
    @Operation(summary = "Get ProductOrder Data",
            tags = {"ProductOrder API"},
            description = "Search ProductOrder data using id")
    @GetMapping(
    		value = USER + PRODUCTORDER
    		//, produces = {APPLICATION_JSON_VALUE}
    		)
    public ProductOrderDto  getProductOrder(@RequestParam(name = "" )Long poId) {
    	ProductOrder productOrder = productOrderService.getProductOrderBypoId(poId);
    	ProductOrderDto productOrderDto = new ProductOrderDto();
    	
    	productOrderDto.setAmount(productOrder.getAmount());
    	productOrderDto.setArtwork(productOrder.getArtwork());
    	productOrderDto.setPoId(productOrder.getId());
    	productOrderDto.setItem(productOrder.getItem());
    	productOrderDto.setStatus(productOrder.getStatus());
    	productOrderDto.setUnitPrice(productOrder.getUnitPrice());
    	
        return productOrderDto;
    } 
    
    //나의 발주정보 전체 조회
    @Operation(summary = "Get Supplier's ProductOrder List Data",
            tags = {"ProductOrder API"},
            description = "Get Supplier's ProductOrder List Data by userid")
    @GetMapping(
    		value = USER + PRODUCTORDER + ALL
    		//, produces = {APPLICATION_JSON_VALUE}
    		)
    public List<ProductOrderDto> getMyProductOrder(@PathVariable(value = "tenantId") String userId) {
    	List<ProductOrder> productOrder = productOrderService.getProductOrderBySupplierName(userId);
    	List<ProductOrderDto> productOrderDtoList = new ArrayList<>();
    	for(ProductOrder order : productOrder) {
    	    ProductOrderDto dto = new ProductOrderDto();
    	    dto.setPoId(order.getId());
    	    dto.setAmount(order.getAmount());
    	    dto.setUnitPrice(order.getUnitPrice());
    	    dto.setStatus(order.getStatus());
    	    dto.setArtwork(order.getArtwork());
    	    dto.setItem(order.getItem());
    	    productOrderDtoList.add(dto);
    	}
        return productOrderDtoList;
    }
    
    //발주 정보 업데이트
    @Operation(summary = "Update ProductOrder Data",
            tags = {"ProductOrder API"},
            description = "Update ProductOrder data")
    @PutMapping(
    		value = USER + PRODUCTORDER + SAVE
            //,produces = {APPLICATION_JSON_VALUE}
    		)
    public ResponseEntity<?> updateProductOrder(@PathVariable(value = "tenantId") String userId, @RequestParam("poId") Long poid, ProductOrderStatus status) {
    	productOrderService.updateProductOrderStatus(userId, poid, status);
    	return ResponseEntity.ok(null);
    	
    }
   

}
