package com.grimpanda.delivery.interfaces.controller;

import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ORDER;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ORDERDELIVERY;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.USER;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grimpanda.delivery.domain.order.model.Order;
import com.grimpanda.delivery.domain.order.service.OrderService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(ORDERDELIVERY)
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Operation(summary = "Get Orders After Payment Data",
            tags = {"Order API"},
            description = "Get Orders After Payment Data")
    @GetMapping(value = USER + ORDER)
    public ResponseEntity<?> getOrdersAfterPayment(@PathVariable(value="tenantId") String userId) {
    	
    	List<Order> orderList = orderService.getOrdersAfterPaymentByBuyerName(userId);
        return ResponseEntity.ok(orderList);
    }

    @Operation(summary = "Get Order After Payment Data",
            tags = {"Order API"},
            description = "Get Order After Payment Data")
    @GetMapping(value = USER + ORDER +"/{orderId}")
    public ResponseEntity<?> getOrderAfterPaymentByOrderId(@PathVariable(value="tenantId") String userId, @PathVariable Long orderId) {
    	
    	Optional<Order> order = orderService.getOrderAfterPaymentByOrderId(orderId);
        return ResponseEntity.of(order);
    }
}
