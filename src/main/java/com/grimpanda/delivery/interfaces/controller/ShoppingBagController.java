package com.grimpanda.delivery.interfaces.controller;

import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ORDERDELIVERY;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.SAVE;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.SHOPPINGBAG;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.USER;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grimpanda.delivery.common.feign.member.MemberFeignClient;
import com.grimpanda.delivery.domain.shoppingbag.model.Artwork;
import com.grimpanda.delivery.domain.shoppingbag.model.Buyer;
import com.grimpanda.delivery.domain.shoppingbag.model.Item;
import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;
import com.grimpanda.delivery.domain.shoppingbag.service.ShoppingBagService;
import com.grimpanda.delivery.interfaces.dto.LoginResultDto;
import com.grimpanda.delivery.interfaces.dto.ProductDto;
import com.grimpanda.delivery.interfaces.dto.ShoppingbagInfoDto;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(ORDERDELIVERY)
@RequiredArgsConstructor
public class ShoppingBagController {
    
    @Autowired
    private ShoppingBagService shoppingBagService;
    
    
    private final MemberFeignClient memberFeignClient;

    @Operation(summary = "Get Shopping Bag Info",
            tags = {"ShoppingBag API"},
            description = "Get Shopping Bag Info")
    @GetMapping(
    		value = USER + SHOPPINGBAG
    		)
    public ResponseEntity<?> getShoppingBag(@PathVariable(value = "tenantId") String userId) {
        List<ShoppingBagProduct> shoppingBag = shoppingBagService.getShoppingBagProductsByName(userId);
        return ResponseEntity.ok(shoppingBag);
    }

    @Operation(summary = "Add Product Data To Shopping Bag",
            tags = {"ShoppingBag API"},
            description = "Add Product Data To Shopping Bag")
    @PostMapping(
    		value = USER + SHOPPINGBAG + SAVE
    		)
    public ResponseEntity<?> addProduct(
    		@PathVariable(value = "tenantId") String userId,
    		@RequestBody ProductDto product) {
    	
    	LoginResultDto buyerInfo = memberFeignClient.getMember(userId);

    	Buyer buyer = Buyer.builder()
        		.name(buyerInfo.getNickname())
        		.hpNo(buyerInfo.getHpNo())
        		.email(buyerInfo.getEmail())
        		.build();
    	
        ShoppingBagProduct shoppingBagProduct = ShoppingBagProduct.builder()
            .artwork(product.getArtwork())
            .item(product.getItem())
            .unitPrice(product.getUnitPrice())
            .amount(product.getAmount())
            .buyer(buyer)
            .build();

        shoppingBagService.addProductToShoppingBag(shoppingBagProduct);

        return ResponseEntity.ok(null);
    }
    
/*
    @Operation(summary = "receive Product Data from Product",
            tags = {"ShoppingBag API"},
            description = "receive Product Data from Product")
    @PostMapping(value = USER + SHOPPINGBAG + SAVE)
    //@PostMapping("/tenants/{userId}/shopping-bag")
    public ResponseEntity<String> receiveShoppingBag(
            @PathVariable("tenantId") String userId,
            @RequestBody List<ShoppingbagInfoDto.RequestDetail> requestDetails) {

        List<ShoppingBagProduct> shoppingBagProducts = new ArrayList<>();

        //고객정보를 feignClient로 받아온다.
    	LoginResultDto buyerInfo = memberFeignClient.getMember(userId);
    	
        for(ShoppingbagInfoDto.RequestDetail shoppingbag : requestDetails) {
        	
            Artwork artwork = Artwork.builder()
            		.artworkId(Long.valueOf(shoppingbag.getArtwork().getId()))
            		.title(shoppingbag.getArtwork().getTitle())
            		.authorNickname(shoppingbag.getArtwork().getAuthor())
            		.build();
            
            Item item = Item.builder()
            		.itemId(Long.valueOf(shoppingbag.getProduct().getId()))
            		.name(shoppingbag.getProduct().getTitle())
            		.supplierName(shoppingbag.getProduct().getSeller())
            		.build();
            
            Buyer byuer = Buyer.builder()
            		.name(buyerInfo.getNickname())
            		.hpNo(buyerInfo.getHpNo())
            		.email(buyerInfo.getEmail())
            		.build();
            
            //가격은 합산하여 넣는다.
            Long unitPrice = shoppingbag.getArtwork().getPrice() +  shoppingbag.getProduct().getPrice();
            Integer amount = shoppingbag.getAmount() ;
            
        	shoppingBagProducts.add(ShoppingBagProduct.builder()
                    .artwork(artwork)
                    .item(item)
                    .unitPrice(unitPrice)
                    .amount(amount)
                    .buyer(byuer)
        			
        			.build()
        			);
        	
        }

        shoppingBagService.addProductToShoppingBagList(shoppingBagProducts);


        // 성공적으로 처리되었을 경우 응답
        return ResponseEntity.ok("Shopping bag data received and processed successfully.");
    }
*/
    @Operation(summary = "Delete Shopping Bag Info",
            tags = {"ShoppingBag API"},
            description = "Delete Shopping Bag Info")
    @DeleteMapping(value = USER + SHOPPINGBAG + "/{productId}")
    public ResponseEntity<?> removeProduct(@PathVariable(value = "tenantId") String userId, @PathVariable Long productId) {

        List<ShoppingBagProduct> shoppingbag = shoppingBagService.getShoppingBagProductsByName(userId);

        List<ShoppingBagProduct> productsToRemove = shoppingbag.stream().filter(
            shoppingBagProduct -> shoppingBagProduct.getId().equals(productId)
        ).toList();

        shoppingBagService.removeProductsFromShoppingBag(productsToRemove);
        return ResponseEntity.ok(null);
    }

}
