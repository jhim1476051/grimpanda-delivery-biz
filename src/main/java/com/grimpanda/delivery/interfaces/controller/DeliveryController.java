package com.grimpanda.delivery.interfaces.controller;

import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.DELIVERY;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.ORDERDELIVERY;
import static com.grimpanda.delivery.common.constant.Path.V1.DeliveryPath.USER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.grimpanda.delivery.domain.delivery.model.DeliveryInfo;
import com.grimpanda.delivery.domain.delivery.model.DeliveryStatus;
import com.grimpanda.delivery.domain.delivery.service.impl.DeliveryServiceImpl;

import io.swagger.v3.oas.annotations.Operation;


@RestController
@RequestMapping(ORDERDELIVERY)
public class DeliveryController {
    
    @Autowired
    private DeliveryServiceImpl deliveryService;

    @Operation(summary = "Get Delivery Data",
            tags = {"Delivery API"},
            description = "Get Delivery Data")
    @GetMapping(
            value = USER + DELIVERY
            //produces = {APPLICATION_JSON_VALUE}
            )
    public ResponseEntity<?>  getDeliveryData(@PathVariable(value = "tenantId") String userId, @RequestParam(required = false) Long invoiceNum) {
    	DeliveryInfo delivery = deliveryService.getDeliveryData(userId, invoiceNum);
        return ResponseEntity.ok(delivery);
    }
    
/*
    @Operation(summary = "Add Delivery Data",
            tags = {"Delivery API"},
            description = "Add Delivery data")
    @PostMapping()
    public ResponseEntity<?> addDelivery(@RequestBody DeliveryDto delivery) {
    	DeliveryInfo deliveryInfo = DeliveryInfo.builder()
            .product(delivery.getProduct())
            .status(DeliveryStatus.preparation)
            .shippingInfo(delivery.getShippingInfo())
            .buyer(delivery.getBuyer())
            //.buyer(testBuyer)
            .build();

        deliveryService.addDeliveryInfo(deliveryInfo);

        return ResponseEntity.ok(null);
    }
    
    @Operation(summary = "Add Delivery Data",
            tags = {"Delivery API"},
            description = "Add Delivery data")
    @PostMapping(
    		value = USER + DELIVERY + SAVE
            //produces = {APPLICATION_JSON_VALUE}
    		)
    public ResponseEntity<?> addDelivery(
    		@PathVariable(value = "tenantId") String userId,
    		Long orderId) {
        deliveryService.addDeliveryInfo(orderId);

        return ResponseEntity.ok(null);
    }
    */
    @Operation(summary = "Update Delivery Data",
            tags = {"Delivery API"},
            description = "Update Delivery data")
    @PutMapping(
            value = USER + DELIVERY 
            //produces = {APPLICATION_JSON_VALUE}
            )
    public ResponseEntity<?> updateDelivery(Long invoiceNum, DeliveryStatus status) {
    	deliveryService.updateDeliveryStatus(invoiceNum, status);
    	return ResponseEntity.ok(null);
    	
    }

}
