package com.grimpanda.delivery.infrastructures;

public interface TossPaymentsPort {
    
    /*
     * 결제 승인 (https://docs.tosspayments.com/reference#결제-승인)
     */
    public TossPayment approveOrder(String paymentKey, String orderId, Integer amount);

    /*
     * paymentKey로 결제 조회 (https://docs.tosspayments.com/reference#paymentkey로-결제-조회)
     */
    public TossPayment findOrderByPaymentKey(String paymentKey);

    /*
     * orderId로 결제 조회 (https://docs.tosspayments.com/reference#orderid로-결제-조회)
     */
    public TossPayment findOrderByOrderId(String orderId);
}