package com.grimpanda.delivery.infrastructures;

import org.springframework.stereotype.Component;

@Component
public class TossPaymentsFakeImpl implements TossPaymentsPort {

    @Override
    public TossPayment approveOrder(String paymentKey, String orderId, Integer amount) {
        return new TossPayment(paymentKey, "type", orderId, "method", amount, "status", "requestedAt", "approvedAt");
    }

    @Override
    public TossPayment findOrderByPaymentKey(String paymentKey) {
        return new TossPayment(paymentKey, "type", "orderId", "method", 100000, "status", "requestedAt", "approvedAt");
    }

    @Override
    public TossPayment findOrderByOrderId(String orderId) {
        return new TossPayment("paymentKey", "type", orderId, "method", 100000, "status", "requestedAt", "approvedAt");
    }
    
}
