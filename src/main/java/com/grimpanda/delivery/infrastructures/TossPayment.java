package com.grimpanda.delivery.infrastructures;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/*
 * https://docs.tosspayments.com/reference#payment-객체
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class TossPayment {
    private String paymentKey;
    private String type;
    private String orderId;
    private String method;
    private Integer totalAmount;
    private String status;
    private String requestedAt;
    private String approvedAt;
}
