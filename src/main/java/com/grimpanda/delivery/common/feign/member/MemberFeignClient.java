package com.grimpanda.delivery.common.feign.member;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.grimpanda.delivery.common.feign.FeignClientConfig;
import com.grimpanda.delivery.interfaces.dto.LoginResultDto;

@FeignClient(name = "MemberFeignClient", url = "${backend.member.url}", configuration = FeignClientConfig.class)
public interface MemberFeignClient {
    @GetMapping(value = "/api/v1/members/{tenantId}")
    LoginResultDto getMember(
            @PathVariable("tenantId") String userId);
}
