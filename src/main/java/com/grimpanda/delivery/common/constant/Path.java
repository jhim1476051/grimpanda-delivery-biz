package com.grimpanda.delivery.common.constant;

public class Path {
    private static final String NO_CONSTRUCTOR_STRING = "Parent Constant class";

    private Path() {
        throw new IllegalStateException(NO_CONSTRUCTOR_STRING);
    }

    /** The constant ACTUATOR_HEALTH. */
    public static final String ACTUATOR_HEALTH = "/actuator/health";
    /** The constant ACTUATOR. */
    public static final String ACTUATOR = "/actuator/**";
    /** The constant DOCS. */
    public static final String DOCS = "/docs/**";
    /** The constant OPEN_API_DOC. */
    public static final String OPEN_API_DOC = "/v3/api-docs/**";
    /** The constant SWAGGER_UI. */
    public static final String SWAGGER_UI = "/swagger-ui/**";
    /** The constant SWAGGER_UI. */
    public static final String SWAGGER_UI_HTML = "/swagger-ui.html";

    public static final class V1 {

        private V1() {
            throw new IllegalStateException(NO_CONSTRUCTOR_STRING);
        }

        private static final String VERSION = "/api/v1";

        public static final class DeliveryPath {
            private DeliveryPath() {
                throw new IllegalStateException(NO_CONSTRUCTOR_STRING);
            }

            public static final String ORDERDELIVERY = VERSION + "/delivery";

            public static final String TENANT = ORDERDELIVERY + "/tenants/{tenantId}";
            public static final String USER = "/tenants/{tenantId}";

            public static final String PRODUCTORDER = "/productorder";
            public static final String DELIVERY = "/delivery";
            public static final String SHOPPINGBAG = "/shopping-bag";
            public static final String ORDER = "/order";
            public static final String ORDERBEFOREPAYMENT = "/orderbeforepayment";
            public static final String SAVE = "/save";
            public static final String ALL = "/all";
        }

    }
}
