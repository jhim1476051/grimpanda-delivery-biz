package com.grimpanda.delivery.domain.member.model;

public enum MemberType {
    CUSTOMER("고객", 1),
    SUPPLIER("공급사", 2),
    ADMIN("관리자", 0);
	
	private String memberTypeValue; // 인스턴스 필드 추가
	private int memberTypeCode;
 
	// 생성자 추가
	MemberType(String memberTypeValue, int memberTypeCode){
		this.memberTypeValue = memberTypeValue;
		this.memberTypeCode = memberTypeCode;
	}
 
	// 인스턴스 필드 get메서드 추가
	public String getMemberTypeValue() {
		return memberTypeValue; 
	}
	
	// 인스턴스 필드 get메서드 추가
	public int getMemberTypeCode() {
		return memberTypeCode; 
	}

}
