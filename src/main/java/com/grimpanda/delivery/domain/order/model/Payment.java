package com.grimpanda.delivery.domain.order.model;

import com.grimpanda.delivery.infrastructures.TossPayment;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Embeddable
public class Payment {
    private String paymentKey;
    private Integer amount;

    public static Payment of(TossPayment payment) {
        return new Payment(payment.getPaymentKey(), payment.getTotalAmount());
    }
}
