package com.grimpanda.delivery.domain.order.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grimpanda.delivery.common.exception.BizException;
import com.grimpanda.delivery.common.exception.ErrorCode;
import com.grimpanda.delivery.domain.order.model.Buyer;
import com.grimpanda.delivery.domain.order.model.Order;
import com.grimpanda.delivery.domain.order.model.Payment;
import com.grimpanda.delivery.domain.order.model.ProductOrder;
import com.grimpanda.delivery.domain.order.model.Status;
import com.grimpanda.delivery.domain.order.repository.OrderRepository;
import com.grimpanda.delivery.domain.order.service.OrderService;
import com.grimpanda.delivery.domain.order.service.ProductOrderService;
import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;
import com.grimpanda.delivery.domain.shoppingbag.service.ShoppingBagService;
import com.grimpanda.delivery.infrastructures.TossPaymentsPort;
import com.grimpanda.delivery.interfaces.dto.ApproveOrderDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ShoppingBagService shoppingBagService;
    
    @Autowired
    private ProductOrderService productOrderService;

    @Autowired
    private TossPaymentsPort tossPaymentsPort;

    @Override
    public Order saveOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Optional<Order> findOrderBeforePayment(String buyerName) {
        return orderRepository.findOneByBuyerNameAndStatus(buyerName, Status.결제_전);
    }

    @Override
    public Optional<Order> findOrderBeforePaymentByOrderId(Long orderId) {
        return orderRepository.findOneByIdAndStatus(orderId, Status.결제_전);
    }

    @Override
    public void approveOrder(Buyer buyer, ApproveOrderDto approveOrderDto) {
        Order orderBeforePayment = orderRepository.findOneByIdAndStatus(approveOrderDto.getOrderId(), Status.결제_전).get();
        
        String userId = buyer.getName();
    	if(!userId.equals( orderBeforePayment.getBuyer().getName())){
    		log.error(
                    "Failed to delete the content(id={}). 접근 권한이 없는 사용자 입니다.",
                    userId);
            throw new BizException(ErrorCode.INVALID_PARAMETER, "접근 권한이 없는 사용자 입니다.");
    	}
        
        List<ShoppingBagProduct> shoppingBagProducts = shoppingBagService.getShoppingBagProductsByName(userId);

        List<ProductOrder> products = shoppingBagProducts
            .stream()
            .map(shoppingBagProduct -> ProductOrder.of(shoppingBagProduct, orderBeforePayment))
            .collect(Collectors.toList());

        Payment payment = Payment.of(
            tossPaymentsPort.approveOrder(
                approveOrderDto.getPaymentKey(),
                approveOrderDto.getTossPaymentOrderId(), 
                approveOrderDto.getAmount()
            )
        );
        
        Order orderAfterPayment = orderBeforePayment.approveOrder(products, payment);

        orderRepository.save(orderAfterPayment);

        shoppingBagService.removeProductsFromShoppingBag(shoppingBagProducts);

    }

    @Override
    public List<Order> getOrdersAfterPaymentByBuyerName(String buyerName) {
        return orderRepository.findAllByBuyerNameAndStatus(buyerName, Status.결제_후);
    }

    @Override
    public Optional<Order> getOrderAfterPaymentByOrderId(Long orderId) {
        return orderRepository.findOneByIdAndStatus(orderId, Status.결제_후);
    }
    
}
