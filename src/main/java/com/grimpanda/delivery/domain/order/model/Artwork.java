package com.grimpanda.delivery.domain.order.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Getter
public class Artwork {
    private Long artworkId;
    private String title;
    //private String contentPath;
    private String authorNickname;

    static Artwork of(com.grimpanda.delivery.domain.shoppingbag.model.Artwork artwork) {
        return new Artwork(
            artwork.getArtworkId(),
            artwork.getTitle(),
            //artwork.getContentPath(),
            artwork.getAuthorNickname()
        );
    }
}
