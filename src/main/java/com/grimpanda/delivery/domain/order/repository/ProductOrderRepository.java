package com.grimpanda.delivery.domain.order.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grimpanda.delivery.domain.order.model.ProductOrder;
import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;

public interface ProductOrderRepository extends JpaRepository<ProductOrder, Long> {

	ProductOrder findProductOrderById(Long id);
	List<ProductOrder> findAllById(Long orderId);
	List<ProductOrder> findAllByItem_SupplierName(String userId);
	
}
