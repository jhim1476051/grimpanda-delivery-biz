package com.grimpanda.delivery.domain.order.model;

public enum ProductOrderStatus {
	preparation("제작중", 1),
	completed("제작완료", 2),
	//delivery("배송요청", 3),
	canceled("제작취소", 0);

 
	private String statusValue; // 인스턴스 필드 추가
	private int statusCode;
 
	// 생성자 추가
	ProductOrderStatus(String statusValue, int statusCode){
		this.statusValue = statusValue;
		this.statusCode = statusCode;
	}
 
	// 인스턴스 필드 get메서드 추가
	public String getStatusValue() {
		return statusValue; 
	}
	
	// 인스턴스 필드 get메서드 추가
	public int getStatusCode() {
		return statusCode; 
	}

}
