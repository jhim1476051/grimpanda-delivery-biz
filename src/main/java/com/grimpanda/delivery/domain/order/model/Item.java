package com.grimpanda.delivery.domain.order.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Getter
public class Item {
    private Long itemId;
    private String name;
   // private String imagePath;
//    private Long supplierId;
    private String supplierName;

    static Item of(com.grimpanda.delivery.domain.shoppingbag.model.Item item) {
        return new Item(
            item.getItemId(),
            item.getName(),
            //item.getImagePath(),
            //item.getSupplierId(),
            item.getSupplierName()
        );
    }
}
