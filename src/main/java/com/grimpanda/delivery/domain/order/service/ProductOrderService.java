package com.grimpanda.delivery.domain.order.service;

import java.util.List;

import com.grimpanda.delivery.domain.order.model.ProductOrder;
import com.grimpanda.delivery.domain.order.model.ProductOrderStatus;

public interface ProductOrderService {
	
	public ProductOrder getProductOrderBypoId(Long poId);
	public List<ProductOrder> getProductOrderBySupplierName(String userId);
    public ProductOrder updateProductOrderStatus( String userId, Long poId, ProductOrderStatus status);

}
