package com.grimpanda.delivery.domain.order.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.grimpanda.delivery.domain.order.model.Buyer;
import com.grimpanda.delivery.domain.order.model.Order;
import com.grimpanda.delivery.interfaces.dto.ApproveOrderDto;

@Service
public interface OrderService {
    public Optional<Order> findOrderBeforePayment(String buyerName);

    public Optional<Order> findOrderBeforePaymentByOrderId(Long orderId);
    
    public Order saveOrder(Order order);

    public void approveOrder(Buyer buyer, ApproveOrderDto approveOrderDto);

    public List<Order> getOrdersAfterPaymentByBuyerName(String buyerName);

    public Optional<Order> getOrderAfterPaymentByOrderId(Long orderId);
}
