package com.grimpanda.delivery.domain.order.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
public class ProductOrder {

    @Id @GeneratedValue
    @Column(name="PO_ID")
    private Long id;
    private Artwork artwork; 
    private Item item;
    private Long unitPrice;
    private Integer amount;
    
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ORDER_ID") // 이 부분이 올바르게 설정되어 있어야 함
    private Order order;
    
    @Enumerated(EnumType.STRING)
    private @Setter ProductOrderStatus status;

    public static ProductOrder of(ShoppingBagProduct shoppingBagProduct, Order order) {
        return new ProductOrder(
            null,
            Artwork.of(shoppingBagProduct.getArtwork()),
            Item.of(shoppingBagProduct.getItem()),
            shoppingBagProduct.getUnitPrice(),
            shoppingBagProduct.getAmount(),
            order,
            ProductOrderStatus.preparation 
        );
    }
}
