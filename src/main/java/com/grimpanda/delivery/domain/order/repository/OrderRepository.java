package com.grimpanda.delivery.domain.order.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grimpanda.delivery.domain.order.model.Order;
import com.grimpanda.delivery.domain.order.model.Status;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Optional<Order> findOneByBuyerNameAndStatus(String buyerName, Status status);

    Optional<Order> findOneByIdAndStatus(Long orderId, Status status);

    List<Order> findAllByBuyerNameAndStatus(String buyerName, Status status);    
    //Optional<Order> findByProductOrder_poId (Long poId);
}
