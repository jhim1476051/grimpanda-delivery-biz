package com.grimpanda.delivery.domain.order.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grimpanda.delivery.common.exception.BizException;
import com.grimpanda.delivery.common.exception.ErrorCode;
import com.grimpanda.delivery.domain.delivery.service.impl.DeliveryServiceImpl;
import com.grimpanda.delivery.domain.order.model.ProductOrder;
import com.grimpanda.delivery.domain.order.model.ProductOrderStatus;
import com.grimpanda.delivery.domain.order.repository.OrderRepository;
import com.grimpanda.delivery.domain.order.repository.ProductOrderRepository;
import com.grimpanda.delivery.domain.order.service.ProductOrderService;
import com.grimpanda.delivery.domain.shoppingbag.repository.ShoppingBagRepository;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductOrderServiceImpl implements ProductOrderService{

    @Autowired
    ProductOrderRepository productOrderRepository;
    
    @Autowired
    ShoppingBagRepository shoppingBagRepository;
    
    @Autowired
    OrderRepository orderRepository;
    
    @Autowired
    private DeliveryServiceImpl deliveryService;

    @Override
    public ProductOrder getProductOrderBypoId(Long poId) {
    	System.out.println("========================== id service : "+poId);
        return productOrderRepository.findProductOrderById(poId);
    }
    
    @Override
    public List<ProductOrder> getProductOrderBySupplierName(String userId) {
    	System.out.println("========================== id service : "+userId);
        return productOrderRepository.findAllByItem_SupplierName(userId);
    }

    
    @Override
    public ProductOrder updateProductOrderStatus(String userId, Long poId, ProductOrderStatus status) {
    	
    	ProductOrder productOrder = productOrderRepository.findProductOrderById(poId);
    	
    	if(!userId.equals( productOrder.getItem().getSupplierName())){
    		log.error(
                    "Failed to delete the content(id={}). 접근 권한이 없는 사용자 입니다.",
                    userId);
            throw new BizException(ErrorCode.INVALID_PARAMETER, "접근 권한이 없는 사용자 입니다.");
    	}
    	
    	productOrder.setStatus(status);
    	
    	productOrderRepository.save(productOrder);
    	
    	//제작 완료시 배송요청
    	if(2 == productOrder.getStatus().getStatusCode()) {
    		deliveryService.addDeliveryInfo(productOrder.getId());
    	}
    	return productOrder;
    }

}
