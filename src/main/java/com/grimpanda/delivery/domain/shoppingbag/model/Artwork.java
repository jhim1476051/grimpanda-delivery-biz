package com.grimpanda.delivery.domain.shoppingbag.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Getter
@Builder
public class Artwork {
    private Long artworkId;
    private String title;
    private String authorNickname;
    //private Boolean artworkAvailableYn;
}
