package com.grimpanda.delivery.domain.shoppingbag.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Data
@Builder
public class Item {
    private Long itemId;
    @Column(name="itemName")
    private String name;
    //private String imagePath;
    //private Long supplierId;
    private String supplierName;
    //private Boolean itemAvailableYn;
}
