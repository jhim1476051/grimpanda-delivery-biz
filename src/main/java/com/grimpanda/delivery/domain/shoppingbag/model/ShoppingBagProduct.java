package com.grimpanda.delivery.domain.shoppingbag.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class ShoppingBagProduct {

    @Id @GeneratedValue
    private Long id;
    private Artwork artwork;
    private Item item;
    private Long unitPrice;
    private Integer amount;
    private Buyer buyer; 

}
