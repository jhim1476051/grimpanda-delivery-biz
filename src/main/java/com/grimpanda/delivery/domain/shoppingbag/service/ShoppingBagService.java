package com.grimpanda.delivery.domain.shoppingbag.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;

@Service
public interface ShoppingBagService {
    //public List<ShoppingBagProduct> getShoppingBagProductsByMemberId(Long memberId);
	public List<ShoppingBagProduct> getShoppingBagProductsByName(String name);
    public void addProductToShoppingBag( ShoppingBagProduct product);
    public void addProductToShoppingBagList(List<ShoppingBagProduct> product);
    public void removeProductFromShoppingBag(ShoppingBagProduct product);
    public void removeProductsFromShoppingBag(List<ShoppingBagProduct> products);
}
