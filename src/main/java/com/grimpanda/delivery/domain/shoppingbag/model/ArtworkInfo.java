package com.grimpanda.delivery.domain.shoppingbag.model;


import java.util.Date;

import org.hibernate.annotations.DynamicUpdate;

import com.grimpanda.delivery.common.util.ModelMapperProvider;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@DynamicUpdate
public class ArtworkInfo {

    private static final ModelMapperProvider MODEL_MAPPER_PROVIDER;

    static {
        MODEL_MAPPER_PROVIDER = ModelMapperProvider.getInstance();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String description;
    private Long price;
    private String contentId;

    private String author;
    
    private Date createdAt;

}
