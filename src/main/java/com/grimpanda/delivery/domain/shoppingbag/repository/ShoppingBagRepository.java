package com.grimpanda.delivery.domain.shoppingbag.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;

public interface ShoppingBagRepository extends JpaRepository<ShoppingBagProduct, Long> {

    //List<ShoppingBagProduct> findAllByBuyerMemberId(Long memberId);
    List<ShoppingBagProduct> findAllByBuyerName(String name);
    
}
