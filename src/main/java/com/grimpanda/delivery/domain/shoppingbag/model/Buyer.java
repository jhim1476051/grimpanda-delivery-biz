package com.grimpanda.delivery.domain.shoppingbag.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Getter
@Builder
public class Buyer {
	//private Long memberId;
    @Column(name = "nickName")
    private String name;
    private String hpNo;
    private String email;
}
