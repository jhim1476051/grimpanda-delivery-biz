package com.grimpanda.delivery.domain.shoppingbag.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grimpanda.delivery.domain.shoppingbag.model.ShoppingBagProduct;
import com.grimpanda.delivery.domain.shoppingbag.repository.ShoppingBagRepository;
import com.grimpanda.delivery.domain.shoppingbag.service.ShoppingBagService;

@Service
public class ShoppingBagServiceImpl implements ShoppingBagService {

    @Autowired
    ShoppingBagRepository shoppingBagRepository;

    /*
    @Override
    public List<ShoppingBagProduct> getShoppingBagProductsByMemberId(Long memberId) {
        return shoppingBagRepository.findAllByBuyerMemberId(memberId);
    }*/
    
    @Override
    public List<ShoppingBagProduct> getShoppingBagProductsByName(String name) {
        return shoppingBagRepository.findAllByBuyerName(name);
    }

    @Override
    public void addProductToShoppingBag(ShoppingBagProduct product) {
        shoppingBagRepository.save(product);
    }
    
    @Override
    public void addProductToShoppingBagList(List<ShoppingBagProduct> product) {
        shoppingBagRepository.saveAll(product);
    }

    @Override
    public void removeProductFromShoppingBag(ShoppingBagProduct product) {
        shoppingBagRepository.delete(product);
    }

    @Override
    public void removeProductsFromShoppingBag(List<ShoppingBagProduct> products) {
        shoppingBagRepository.deleteAll(products);
    }
    
}
