package com.grimpanda.delivery.domain.delivery.service;

import com.grimpanda.delivery.domain.delivery.model.DeliveryInfo;
import com.grimpanda.delivery.domain.delivery.model.DeliveryStatus;

public interface DeliveryService {
	public DeliveryInfo getDeliveryData(String name, Long invoiceNum);
    //public DeliveryInfo addDeliveryInfo(DeliveryInfo product);
	public DeliveryInfo addDeliveryInfo(Long id);
    public DeliveryInfo updateDeliveryStatus( Long invoiceNum, DeliveryStatus status);
}
