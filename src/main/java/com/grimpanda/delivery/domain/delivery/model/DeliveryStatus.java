package com.grimpanda.delivery.domain.delivery.model;

public enum DeliveryStatus {
	preparation("상품준비중", 1),
	start("배송시작", 2),
	proceeding("배송중", 3),
	completed("배송완료", 4),
	canceled("배송취소", 5);

 
	private String statusValue; // 인스턴스 필드 추가
	private int statusCode;
 
	// 생성자 추가
	DeliveryStatus(String statusValue, int statusCode){
		this.statusValue = statusValue;
		this.statusCode = statusCode;
	}
 
	// 인스턴스 필드 get메서드 추가
	public String getStatusValue() {
		return statusValue; 
	}
	
	// 인스턴스 필드 get메서드 추가
	public int getStatusCode() {
		return statusCode; 
	}

}
