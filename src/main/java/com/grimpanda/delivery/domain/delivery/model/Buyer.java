package com.grimpanda.delivery.domain.delivery.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Data
@Builder
public class Buyer {
    @Column(name = "buyerName")
    private String name;
    private String hpNo;
    private String email;
}
