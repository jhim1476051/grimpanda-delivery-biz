package com.grimpanda.delivery.domain.delivery.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Getter
@Builder
public class Address {
    private String zipCode;
    private String roadName;
    private String detail;
}
