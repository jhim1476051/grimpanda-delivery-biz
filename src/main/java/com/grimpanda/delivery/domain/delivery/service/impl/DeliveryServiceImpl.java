package com.grimpanda.delivery.domain.delivery.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grimpanda.delivery.common.feign.member.MemberFeignClient;
import com.grimpanda.delivery.domain.delivery.model.Address;
import com.grimpanda.delivery.domain.delivery.model.Buyer;
import com.grimpanda.delivery.domain.delivery.model.DeliveryInfo;
import com.grimpanda.delivery.domain.delivery.model.DeliveryStatus;
import com.grimpanda.delivery.domain.delivery.model.Product;
import com.grimpanda.delivery.domain.delivery.model.ShippingInfo;
import com.grimpanda.delivery.domain.delivery.repository.DeliveryRepository;
import com.grimpanda.delivery.domain.delivery.service.DeliveryService;
import com.grimpanda.delivery.domain.order.model.ProductOrder;
import com.grimpanda.delivery.domain.order.repository.OrderRepository;
import com.grimpanda.delivery.domain.order.repository.ProductOrderRepository;
import com.grimpanda.delivery.interfaces.dto.LoginResultDto;

@Service
public class DeliveryServiceImpl implements DeliveryService{

    @Autowired
    DeliveryRepository deliveryRepository;
    
    @Autowired
    OrderRepository orderRepository;
    
    @Autowired
    ProductOrderRepository productOrderRepository;
    
    @Autowired
    private MemberFeignClient memberFeignClient;
    
    @Override
    public DeliveryInfo getDeliveryData(String name, Long invoiceNum) {
    	DeliveryInfo result = new DeliveryInfo();
    	LoginResultDto buyerInfo = memberFeignClient.getMember(name); 
    	if("공급사".equals(buyerInfo.getMemberType().getMemberTypeValue())) {
    		if(null == invoiceNum) {
    			result = deliveryRepository.findAllBySupplierName(name);
    		}else {
    			result = deliveryRepository.findAllByInvoiceNumAndSupplierName(invoiceNum, name);
    		}
    		
    	}else {
    		if(null == invoiceNum) {
    			result = deliveryRepository.findAllByBuyerName(name);
    		}else {
    			result = deliveryRepository.findAllByInvoiceNumAndBuyerName(invoiceNum, name);
    		}
    		
    	}
 
        return result;
    }

    @Override
    public DeliveryInfo addDeliveryInfo(Long poId) {
    	
    	/*
    private String artworkTitle;
    private String itemName;
    private Integer unitPrice;
    private Integer amount;
    	 * */
    	
    	/*
    	Optional<Order> order = orderRepository.findById(orderId);
    	List<ProductOrder> productOrder = productOrderRepository.findAllById(orderId);
    	
    	ProductOrder productOrderData = new ProductOrder();

    	for(int i = 0 ; i < productOrder.size(); i++) {
    		if(2 == productOrder.get(i).getStatus().getStatusCode()) {
    			productOrderData = productOrder.get(i);
    		}
    	}
    	*/
    	//List<ProductOrder> productOrder = productOrderReposiemfdtory.findAllById(orderId);
    	ProductOrder productOrder = productOrderRepository.findProductOrderById(poId);
    	//Optional<Order> order = orderRepository.findByProductOrder_poId(poId);
    	//Optional<Order> order = orderRepository.findById(poId);
    	Address address = Address.builder()
    			.roadName(productOrder.getOrder().getShippingInfo().getAddress().getRoadName())
    			.detail(productOrder.getOrder().getShippingInfo().getAddress().getDetail())
    			.zipCode(productOrder.getOrder().getShippingInfo().getAddress().getZipCode())
    			.build();
    	//package com.grimpanda.delivery.domain.delivery.model;
    			
    	Product product = Product.builder()
    			.artworkTitle(productOrder.getArtwork().getAuthorNickname())
    			.itemName(productOrder.getItem().getName())
    			.unitPrice(productOrder.getUnitPrice())
    			.amount(productOrder.getAmount())
    			.build();
    	ShippingInfo shippingInfo = ShippingInfo.builder()
    			.address(address)
    			.remark(productOrder.getOrder().getShippingInfo().getRemark())
    			.build();
    	Buyer buyer = Buyer.builder()
    			.name(productOrder.getOrder().getBuyer().getName())
    			.hpNo(productOrder.getOrder().getBuyer().getHpNo())
    			.email(productOrder.getOrder().getBuyer().getEmail())
    			.build();
    	
    	
    	DeliveryInfo deliveryInfo = DeliveryInfo.builder()
    			.product(product)
    			.shippingInfo(shippingInfo)
    			.buyer(buyer)
    			.status(DeliveryStatus.preparation)
    			.build();
    	
        return deliveryRepository.save(deliveryInfo);
    }
    /*
    @Override
    public DeliveryInfo addDeliveryInfo(DeliveryInfo deliveryInfo) {
        return deliveryRepository.save(deliveryInfo);
    }
     * */
    
    @Override
    public DeliveryInfo updateDeliveryStatus(Long invoiceNum, DeliveryStatus status) {
    	
    	Optional <DeliveryInfo> deliveryInfo = deliveryRepository.findById(invoiceNum);
    	DeliveryInfo deliveryInfo2 = deliveryInfo.get();
    	if(deliveryInfo.isEmpty()) {
    		System.out.println("현재 배송 진행중인 상품이 아님");
    		return deliveryInfo2;
    	}
    	deliveryInfo2.setStatus(status);
    	return deliveryRepository.save(deliveryInfo2);
    }

}
