# app/domain/delivery
- 배송(Delivery) 도메인에 해당하는 파일을 추가하는 폴더입니다.
  - Entity, Value Object, Enum 등의 도메인 Model 파일
  - Input Port 에 해당하는 Service 파일
  - Use Case 에 해당하는 Service Impl 파일
  - Output Port 에 해당하는 Repository 파일
- 각각 model, service, service/impl, repository 폴더로 구분할 수 있습니다.