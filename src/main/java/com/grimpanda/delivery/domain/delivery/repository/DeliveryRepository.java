package com.grimpanda.delivery.domain.delivery.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grimpanda.delivery.domain.delivery.model.DeliveryInfo;

public interface DeliveryRepository extends JpaRepository<DeliveryInfo, Long> {

	DeliveryInfo findAllBySupplierName(String supplierName);    
	DeliveryInfo findAllByInvoiceNumAndSupplierName(Long invoiceNum, String supplierName);
	
	DeliveryInfo findAllByBuyerName(String name);  
	DeliveryInfo findAllByInvoiceNumAndBuyerName(Long invoiceNum, String name);      
}
