package com.grimpanda.delivery.domain.delivery.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Data
@Builder
public class Product {
    private String artworkTitle;
    private String itemName;
    private Long unitPrice;
    private Integer amount;
}
