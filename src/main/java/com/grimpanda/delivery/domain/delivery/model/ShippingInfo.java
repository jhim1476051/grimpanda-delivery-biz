package com.grimpanda.delivery.domain.delivery.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Data
@Builder
public class ShippingInfo {
    private Address address;
    private String remark;
}
