package com.grimpanda.delivery;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 * 배포 환경 테스트를 위한 샘플 Controller 입니다.
 * 프로젝트 완료 시 삭제해야 합니다.
 */
@RestController
@RequestMapping("/")
public class SampleController {
    
    @GetMapping()
    public String sample() {
        return "Hello";
    }
    @GetMapping("/jack")
    public String sample2() {
        return "Hello jack";
    }
}
